package ru.t1.shipilov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.shipilov.tm.util.TerminalUtil;

public final class TaskUpdateByIdCommand extends AbstractTaskCommand {

    @NotNull
    private final String NAME = "task-update-by-id";

    @NotNull
    private final String DESCRIPTION = "Update task by Id.";

    @Override
    public void execute() {
        @NotNull final String userId = getUserId();
        System.out.println("[UPDATE TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        getTaskService().updateById(userId, id, name, description);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
