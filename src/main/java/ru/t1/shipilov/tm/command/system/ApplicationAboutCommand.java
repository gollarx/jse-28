package ru.t1.shipilov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.shipilov.tm.api.service.IPropertyService;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @NotNull
    private final String ARGUMENT = "-a";

    @NotNull
    private final String NAME = "about";

    @NotNull
    private final String DESCRIPTION = "Show developer info.";

    @Override
    public void execute() {
        @NotNull final IPropertyService service = getPropertyService();
        System.out.println("[APPLICATION]");
        System.out.println("NAME: " + service.getApplicationName());
        System.out.println();

        System.out.println("[ABOUT]");
        System.out.println("AUTHOR: " + service.getAuthorName());
        System.out.println("E-MAIL: " + service.getAuthorEmail());
        System.out.println();

        System.out.println("[GIT]");
        System.out.println("BRANCH: " + service.getGitBranch());
        System.out.println("COMMIT ID: " + service.getGitCommitId());
        System.out.println("COMMITTER: " + service.getGitCommitterName());
        System.out.println("E-MAIL: " + service.getGitCommitterEmail());
        System.out.println("MESSAGE: " + service.getGitCommitMessage());
        System.out.println("TIME: " + service.getGitCommitTime());
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
